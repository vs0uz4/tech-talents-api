# Api

To start your Phoenix app:

  * Run docker containers with `docker-compose up` or `docker-compose up -d`
  * Install dependencies with `docker-compose run app mix deps.get`
  * Create your database with `docker-compose run app mix ecto.create`
  * Execute your migrations with `docker-compose run app mix ecto.migrate`
  * Restart docker container with Phoenix endpoint with `docker-compose restart app`
  * Restart docker container with tests `docker-compose restart test`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
