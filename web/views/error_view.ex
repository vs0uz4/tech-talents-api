defmodule Api.ErrorView do
  use Api.Web, :view
  use JaSerializer.PhoenixView

  def render("401.json", _assigns) do
    %{title: "Unauthorized", code: 401}
    |> JaSerializer.ErrorSerializer.format
  end

  def render("403.json", _assigns) do
    %{title: "Forbidden", code:  403}
    |> JaSerializer.ErrorSerializer.format
  end
 
  def render("404.json", _assigns) do
    %{title: "Page not found", code: 404}
    |> JaSerializer.ErrorSerializer.format
  end

  def render("415.json", _assigns) do
    %{title: "Unsupported content type", code: 415}
    |> JaSerializer.ErrorSerializer.format
  end

  def render("500.json", _assigns) do
    %{title: "Internal server error", code: 500}
    |> JaSerializer.ErrorSerializer.format
  end

  def render("505.json", _assigns) do
    %{title: "Version not supported", code: 505}
    |> JaSerializer.ErrorSerializer.format
  end

  def template_not_found(_template, assigns) do
    render "500.json", assigns
  end
end
