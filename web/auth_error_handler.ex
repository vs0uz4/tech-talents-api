defmodule Api.AuthErrorHandler do
  use Api.Web, :controller

  def unauthenticated(conn, params) do
    conn
    |> put_status(401)
    |> render(Api.ErroView, "401.json")
  end

  def unauthorized(conn, params) do
    conn
    |> put_status(403)
    |> render(Api.ErroView, "403.json")
  end
end
