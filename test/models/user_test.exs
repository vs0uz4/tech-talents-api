defmodule Api.UserTest do
  use Api.ModelCase

  alias Api.User

  @valid_attrs %{
    email: "cristiano.codelab@gmail.com", 
    password: "ph03n1xd3v3l0p3r", 
    password_confirmation: "ph03n1xd3v3l0p3r"
  }

  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "mis-matched password_confirmation doesn't work" do
    changeset = User.changeset(%User{}, %{
      email: "cristiano.codelab@gmail.com",
      password: "ph03n1xd3v3l0p3r",
      password_confirmation: "ph03n1xD3v3l0p3r"
    })
    refute changeset.valid?
  end

  test "missing password_confirmation doesn't work" do
    changeset = User.changeset(%User{}, %{
      email: "cristiano.codelab@gmail.com",
      password: "ph03n1xd3v3l0p3r"
    })
    refute changeset.valid?
  end

  test "short password doesn't work" do
    changeset = User.changeset(%User{}, %{
      email: "cristiano.codelab@gmailcom",
      password: "123",
      password_confirmation: "123"
    })
    refute changeset.valid?
  end
end
