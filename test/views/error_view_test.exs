defmodule Api.ErrorViewTest do
  use Api.ConnCase, async: true

  import Phoenix.View

  test "renders 401.json" do
    assert render(Api.ErrorView, "401.json", []) ==
           %{title: "Unauthorized", code: 401}
           |> JaSerializer.ErrorSerializer.format
  end
  
  test "renders 403.json" do
    assert render(Api.ErrorView, "403.json", []) ==
           %{title: "Forbidden", code:  403}
           |> JaSerializer.ErrorSerializer.format
  end

  test "renders 404.json" do
    assert render(Api.ErrorView, "404.json", []) ==
           %{title: "Page not found", code: 404}
           |> JaSerializer.ErrorSerializer.format
  end

  test "renders 415.json" do
    assert render(Api.ErrorView, "415.json", []) ==
          %{title: "Unsupported content type", code: 415}
          |> JaSerializer.ErrorSerializer.format
  end

  test "render 500.json" do
    assert render(Api.ErrorView, "500.json", []) ==
           %{title: "Internal server error", code: 500}
           |> JaSerializer.ErrorSerializer.format
  end

  test "render 505.json" do
    assert render(Api.ErrorView, "505.json", []) ==
           %{title: "Version not supported", code: 505}
           |> JaSerializer.ErrorSerializer.format
  end
end
